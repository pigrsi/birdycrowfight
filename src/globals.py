import os, pygame
import urllib2

currentState = None
WINDOW_WIDTH = 1024 + 192
WINDOW_HEIGHT = 712
MENU_WIDTH = 512
MENU_HEIGHT = 320
GAME_WIDTH = 1024
GAME_HEIGHT = 712
SAND_HEIGHT = 44
FPS = 60
DEBUG_FONT = None

game_screen_offset = (WINDOW_WIDTH - GAME_WIDTH)/2
menu_screen_offset = (((WINDOW_WIDTH - MENU_WIDTH)/2), ((WINDOW_HEIGHT - MENU_HEIGHT)/2))

screen_shake_offset = [0, 0]

ncv = ncc = nsv = nsc = None  

# For debugging
IS_SERVER = True
try:
    MY_PUBLIC_IP = urllib2.urlopen('http://ip.42.pl/raw').read()
except urllib2.URLError:
    MY_PUBLIC_IP = "Ask Google!"

IP_ADDRESS = ''
import sys
if len(sys.argv) > 1:
    IP_ADDRESS = sys.argv[1]
    sys.stdout.flush()
#    IS_SERVER = False
    
images = {}
def get_image(path):
    global images
    image = images.get(path)
    if image == None:
            canonicalized_path = path.replace('/', os.sep).replace('\\', os.sep)
            image = pygame.image.load(canonicalized_path)
            images[path] = image
    return image
    
def colliderect_edges(a, b):
    return a.left <= b.right and a.right >= b.left and a.top <= b.bottom and a.bottom >= b.top
    
def get_innerscreen_mousepos(innerScreenWidth, innerScreenHeight):
    # If an inner screen is smaller than the window,  its top left origin is different
    pos = pygame.mouse.get_pos()
    return (pos[0] - (WINDOW_WIDTH-innerScreenWidth)/2, pos[1] - (WINDOW_HEIGHT-innerScreenHeight)/2)