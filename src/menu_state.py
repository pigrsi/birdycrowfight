import pygame, sys
from input_controller import Button, ButtonManager, createButton
import event_manager, play_state, play_state_local, network, globals
from twisted.internet import reactor

class MenuState(object):
    def __init__(self, main_screen, eventManager):
        self.main_screen = main_screen
        self.menu_screen = pygame.Surface((globals.MENU_WIDTH, globals.MENU_HEIGHT))
        self.eventManager = eventManager
        
        self.menuBackground = globals.get_image("assets/menu_without_modes.png").convert()
        
        buttonFont = pygame.font.SysFont("calibri", 16)
        buttonList = []
        button = createButton(buttonFont, "Host online game", None, self.startOnlineGame)
        buttonList.append(button)
        button.rect = pygame.Rect(69, 66, 161, 42)
        
        button = createButton(buttonFont, "Start local game", None, self.startLocalGame)
        buttonList.append(button)
        button.rect = pygame.Rect(277, 66, 161, 42)
        
        button = createButton(buttonFont, "Go", None, self.startConnectGame)
        buttonList.append(button)
        button.rect = pygame.Rect(174, 258, 161, 42)
        
        # Add buttons to button manager
        self.buttonManager = ButtonManager(globals.MENU_WIDTH, globals.MENU_HEIGHT, buttonList)
        
        # Textbox text
        self.textboxFont = buttonFont
        self.textboxText = ""
        self.textboxRenderedText = self.textboxFont.render(self.textboxText + "Enter IP address here.", 1, (128,128,128))
        self.textboxTextSize = (0,0)
        self.textboxTextPosition = (18, 218)
        self.showTextCursor = False
        self.showTextCursorTimer = 0.7
        
    def update(self, delta):
        self.buttonManager.update()
        
        # Flash textbox cursor
        self.showTextCursorTimer -= delta
        if self.showTextCursorTimer <= 0:
            self.showTextCursor = not self.showTextCursor
            self.showTextCursorTimer = 0.7
    
    def draw(self):
        # Draw menu background
        self.main_screen.fill((0,0,51))
        self.menu_screen.blit(self.menuBackground, [0, 0])
        # Draw buttons
        self.buttonManager.draw(self.menu_screen)
        # Textbox text
        if self.textboxText == "":
            self.menu_screen.blit(self.textboxRenderedText, (self.textboxTextPosition[0]+4, self.textboxTextPosition[1]))
        else:
            self.menu_screen.blit(self.textboxRenderedText, (self.textboxTextPosition[0], self.textboxTextPosition[1]))
            
        # Draw typing cursor
        if self.showTextCursor:
            pygame.draw.line(self.menu_screen, (0,0,0), 
                            (self.textboxTextPosition[0]+self.textboxTextSize[0]+1, self.textboxTextPosition[1]-2),
                            (self.textboxTextPosition[0]+self.textboxTextSize[0]+1, self.textboxTextPosition[1]+14), 1)
        
    def appendTextbox(self, key):
        if key >= pygame.K_0 and key <= pygame.K_9 and len(self.textboxText) < 15:
            self.textboxText = self.textboxText + str(key - pygame.K_0)
        elif key == pygame.K_PERIOD and len(self.textboxText) < 15:
            self.textboxText = self.textboxText + '.'
        elif key == pygame.K_BACKSPACE or key == pygame.K_DELETE:
            self.textboxText = self.textboxText[:-1]
        else:
            return

        self.showTextCursorTimer = 0.5
        self.showTextCursor = True
        self.textboxTextSize = self.textboxFont.size(self.textboxText)
        self.textboxRenderedText = self.textboxFont.render(self.textboxText, 1, (83, 83, 83))
        
    def startOnlineGame(self):
        pygame.display.set_caption("Birdy Crowfight - Server")
        self.eventManager.Post(event_manager.ChangeStateEvent("PlayState", False))
    
    def startLocalGame(self):
        pygame.display.set_caption("Birdy Crowfight - Local game")
        self.eventManager.Post(event_manager.ChangeStateEvent("PlayStateLocal"))
    
    def startConnectGame(self):
        globals.IP_ADDRESS = self.textboxText
        pygame.display.set_caption("Birdy Crowfight - Client (Connecting to " + globals.IP_ADDRESS + ")")
        self.eventManager.Post(event_manager.ChangeStateEvent("PlayState", True))
        
    def getSecondScreen(self):
        return self.menu_screen
        
    def HandleInput(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP:
            self.buttonManager.checkPressed(event)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_KP_ENTER or event.key == pygame.K_RETURN:
                self.startConnectGame()
            else:
                self.appendTextbox(event.key)