import pygame, math, globals
from input_controller import Button

# Everything about the aiming device. Made up of multiple bits
class AimArrow(object):
    def __init__(self, crowParent, screen, buttonManager):
        # The crow it is following should the crow be moved
        self.crow = crowParent
        # The dotted line thing can't be hardcoded because i might change the crow's size
        self.arcReposition()

        knobUnpressed = globals.get_image("assets/knob_unpressed.png").convert_alpha()
        self.knobButton = Button(False, knobUnpressed, self.knobPressed, self.knobUnpressed)
        self.knobButton.hoveredImage = globals.get_image("assets/knob_hovered.png").convert_alpha()
        self.knobButton.pressedImage = globals.get_image("assets/knob_pressed.png").convert_alpha()
        self.knobButton.dontDraw = True
        buttonManager.add(self.knobButton)
 
        # Initial aiming angle
        self.angle = 1.5*math.pi
        self.knobReposition()
        
        # Power bar
        self.powerBarSprite = pygame.sprite.Sprite()
        self.powerBarSprite.image = globals.get_image("assets/power_bar_empty.png").convert_alpha()
        self.powerBarSprite.rect = self.powerBarSprite.image.get_rect()
        self.powerBarFillingImage = globals.get_image("assets/power_bar_filling.png").convert_alpha()
        
        # Create invisible button that should follow crow sprite
        self.chargeButton = Button(True, None, self.chargeButtonPressed, self.chargeButtonUnpressed)
        # Has no image so set rect manually
        self.chargeButton.rect = self.crow.rect
        buttonManager.add(self.chargeButton)
        
        # Follow mouse when knob is clicked
        self.followMouse = False
        self.keyLeft = False
        self.keyRight = False
        self.keySpace = False
        self.charging = False
        self.chargingFinished = False
        self.fired = True
        self.aboutToFire = False
        self.power = 0
        self.preShotPauseTimer = 1
        
        self.powerBarReposition()
        
    def update(self, gameModel, screen, delta):
        if not self.charging:
            if self.followMouse:
                self.setAngle(True, False)
            elif self.keyLeft or self.keyRight:
                self.setAngle(False, True)
            self.chargeButton.rect = self.crow.rect
        else:
            if not self.chargingFinished:
                # Charge power bar
                if self.power < 1:
                    self.power += 0.01
            else:
                self.aboutToFire = True
                # Take the shot
                if self.preShotPauseTimer > 0:
                    self.preShotPauseTimer -= delta
                else:
                    #self.crow.fire(self.power, self.angle)
                    gameModel.setShot(0, self.power, self.angle, self)
        
        self.arcReposition()
        self.knobReposition()
        self.powerBarReposition()        
        textAngle = "Angle: "+str(self.angle-2*math.pi)
        textPower = "Power: "+str(self.power)
        #labelAngle = globals.DEBUG_FONT.render(textAngle, 1, (0,0,0))
        #labelPower = globals.DEBUG_FONT.render(textPower, 1, (29,29,29))
        #screen.blit(labelAngle, (self.crow.rect.right + 100, self.crow.rect.top))
        #screen.blit(labelPower, (self.crow.rect.right + 100, self.crow.rect.top - 35))
        
    def draw(self, screen, xOffset=0):
        # Draws its own set of sprites
        arcColour = (255,255,255)
        arcRect = pygame.Rect(self.arcLeft+xOffset, self.arcTop, self.arcWidth, self.arcHeight)
        start_angle = 0
        stop_angle = math.pi
        width = 2
        pygame.draw.arc(screen, arcColour, arcRect, start_angle, stop_angle, width)
        # draw the knob on the arc
        screen.blit(self.knobButton.image, (self.knobButton.rect.x+xOffset, self.knobButton.rect.y))
        screen.blit(self.rotatedPowerBar, (self.rotatedPowerBarRect.x+xOffset, self.rotatedPowerBarRect.y))
        
        if self.power > 0:
            screen.blit(self.rotatedFillingBar, (self.rotatedFillingRect.x+xOffset, self.rotatedFillingRect.y))
            
    def arcReposition(self):
        crowRect = self.crow.rect
        self.arcLeft = crowRect.x - crowRect.width*0.5
        self.arcRight = crowRect.x + crowRect.width*1.5
        
        self.arcTop = crowRect.y - crowRect.height*0.5
        self.arcBottom = crowRect.y + crowRect.height*1.5
        self.arcWidth = self.arcRight - self.arcLeft
        self.arcHeight = self.arcBottom - self.arcTop
        
    def knobReposition(self):
        arcCentreX = self.arcLeft + self.arcWidth/2
        arcCentreY = self.arcTop + self.arcHeight/2
        radius = arcCentreX - self.arcLeft
        
        knobLeft = arcCentreX + radius * math.cos(self.angle)
        knobTop = arcCentreY + radius * math.sin(self.angle)
    
        s = self.knobButton.rect
        self.knobButton.rect = pygame.Rect(knobLeft-s.width/2, knobTop-s.height/2, 32, 32)
        
    def powerBarReposition(self):
        arcCentreX = self.arcLeft + self.arcWidth/2
        arcCentreY = self.arcTop + self.arcHeight/2
        # Lower radius than the knob circle so it will be closer to crow
        radius = arcCentreX - self.arcLeft - 16
        
        pbLeft = arcCentreX + radius * math.cos(self.angle)
        pbTop = arcCentreY + radius * math.sin(self.angle)
    
        s = self.powerBarSprite.rect
        self.powerBarSprite.rect = pygame.Rect(pbLeft-s.width/2, pbTop-s.height + s.width/2, 16, 192)

        # Rotation of power bar
        # This is because when moving the image to the right position, it has to
        # be moved left (negatively) if aiming to the left, but since rotatedRect.width
        # can only be larger or equal to powerBarSprite.width, I make it negative with this
        
        # Draw power meter filling
        if self.power > 0:
            #self.powerBarSprite.image.blit(self.powerBarFillingImage, [0, 0, 20, 20])
            
            # The bar starts growing from the bottom, so start with the whole bar cut off
            # and as the power goes up, decrease the cut off amount
            fillingRect = self.powerBarFillingImage.get_rect()
            h = fillingRect.height
            cutoffH = h - (self.power * h)
            pbfImage = self.powerBarFillingImage.subsurface([0,cutoffH, 16,h-cutoffH])
            
            multiplier = 1
            if self.angle <= 1.5*math.pi:
                multiplier = -1
            rotated = pygame.transform.rotozoom(pbfImage, -self.angle*180/math.pi - 90, 1)
            rotatedRect = rotated.get_rect()
            rotatedRect.center = self.powerBarSprite.rect.center
            rotatedRect.move_ip(multiplier*(rotatedRect.width - fillingRect.width)/2, 
                                -(rotatedRect.height - fillingRect.height)/2)
            self.rotatedFillingBar = rotated
            self.rotatedFillingRect = rotatedRect
        multiplier = 1
        if self.angle <= 1.5*math.pi:
            multiplier = -1
        rotated = pygame.transform.rotozoom(self.powerBarSprite.image, -self.angle*180/math.pi - 90, 1)
        rotatedRect = rotated.get_rect()
        rotatedRect.center = self.powerBarSprite.rect.center
        rotatedRect.move_ip(multiplier*(rotatedRect.width - self.powerBarSprite.rect.width)/2, 
                            -(rotatedRect.height - self.powerBarSprite.rect.height)/2)
        self.rotatedPowerBar = rotated
        self.rotatedPowerBarRect = rotatedRect
        
    def knobPressed(self):
        self.followMouse = True
    
    def knobUnpressed(self):
        self.followMouse = False
        
    def chargeButtonPressed(self, fromMouseClick=True):
        self.mouseToCharge = fromMouseClick
        self.charging = True
    
    def chargeButtonUnpressed(self, fromMouseClick=True):
        if fromMouseClick is self.mouseToCharge:
            self.chargingFinished = True
        
    def setAngle(self, useMouse, useKeyboard):
        prevAngle = self.angle
        if useMouse:
            # Set angle relative to mouse position
            mousePos = globals.get_innerscreen_mousepos(globals.GAME_WIDTH, globals.GAME_HEIGHT)
            # Centre of the circle
            origin = self.crow.rect.center
            
            dx = mousePos[0] - origin[0]
            dy = mousePos[1] - origin[1]
            self.angle = math.atan2(dy, dx)

            # Mouse takes priority
            self.keyLeft = False
            self.keyRight = False
            
        elif useKeyboard:
            self.angle -= 2*math.pi
            if self.keyLeft:
                self.angle -= math.pi/128
                if self.angle < -math.pi:
                    self.angle = -math.pi
            if self.keyRight:
                self.angle += math.pi/128
        else:
            return
        # Get the angle using arctan2 and add 2pi so it goes in the right angle
        # If the calculated angle is > 0 then it is pointing down, which is not allowed,
        # so in this case make the knob get stuck on whichever edge it is closest to
        
        # The inner if statements say that if you were on the edge of the arc previously,
        # and you move to the other side but under the arc, then stay there until you are in
        # a nice position again
        if self.angle > 0 and self.angle <= math.pi/2:
            if prevAngle == -math.pi + 2*math.pi:
                self.angle = -math.pi
            else:
                self.angle = 0
        elif self.angle > 0 and self.angle > math.pi/2:
            if prevAngle == 2*math.pi:
                self.angle = 0
            else:
                self.angle = -math.pi
        
        self.angle += 2*math.pi
        
    def reset(self):
        self.charging = False
        self.chargingFinished = False
        self.fired = False
        self.power = 0
        self.preShotPauseTimer = 1
        self.aboutToFire = False
        
    def keyEvent(self, event):
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                self.keyLeft = False
            elif event.key == pygame.K_RIGHT:
                self.keyRight = False
            elif event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                self.keySpace = False
                self.chargeButtonUnpressed(False)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.keyLeft = True
            elif event.key == pygame.K_RIGHT:
                self.keyRight = True
            elif event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                self.keySpace = True
                self.chargeButtonPressed(False)