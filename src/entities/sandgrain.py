import globals, pygame, random

class SandGrain(pygame.sprite.Sprite):
    def __init__(self, x, y, xSpeed, ySpeed):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.gravity = 0.3
        self.xSpeed = xSpeed
        self.ySpeed = ySpeed
        self.untransformed_image = globals.get_image("assets/sand.png").convert_alpha()
        self.image = self.untransformed_image
        self.rect = self.image.get_rect()
        
    def update(self):
        self.ySpeed += self.gravity
        self.x += self.xSpeed
        self.y += self.ySpeed
        self.rect = pygame.Rect(self.x, self.y, 4, 4)
        
        if self.y >= globals.GAME_HEIGHT - globals.SAND_HEIGHT:
            self.kill()
            
class GlassShard(pygame.sprite.Sprite):
    def __init__(self, x, y, xSpeed, ySpeed, type=0):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.gravity = 0.4
        self.xSpeed = xSpeed
        self.ySpeed = ySpeed
        self.untransformed_image = globals.get_image("assets/glassshard" + str(type) + ".png").convert_alpha()
        self.image = self.untransformed_image
        self.rect = self.image.get_rect()
        self.angle = random.random() * 360
        
    def update(self):
        if self.angle >= 360:
            self.angle -= 360
        elif self.angle <= -360:
            self.angle += 360
        
        if self.y+self.rect.height/2 >= globals.GAME_HEIGHT - globals.SAND_HEIGHT:
            # Landed. 
            if not ((self.angle > -8 and self.angle < 8) or (self.angle > 172 and self.angle < 188)):
                if (self.angle > 0 and self.angle < 180) or (self.angle < -180):
                    self.angle -= 6
                else:
                    self.angle += 6
        else:
            # Falling
            self.x += self.xSpeed
            if self.x < 0 or self.x+self.rect.width > globals.GAME_WIDTH:
                self.xSpeed *= -0.3

            self.ySpeed += self.gravity
            self.y += self.ySpeed
            self.angle += self.xSpeed
            
        self.image = pygame.transform.rotate(self.untransformed_image, self.angle)
        self.rect = pygame.Rect(self.x, self.y, self.rect.width, self.rect.height)
        