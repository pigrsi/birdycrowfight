import pygame, math, random
import globals, entities.ball, entities.sandgrain
from entities.divider import Divider

class Crow(pygame.sprite.Sprite):
    def __init__(self, x, y, w, h, flip=False):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.velocityX = 0
        self.velocityY = 0
        self.gravity = 0.3
        self.HP = 100
        self.standing = False
        
        # Set sprite image
        self.image = globals.get_image("assets/crow.png").convert_alpha()
        original_size = self.image.get_rect().size
        self.image = pygame.transform.scale(self.image, (w, h))
        if flip:
            self.image = pygame.transform.flip(self.image, True, False)
        self.normalImage = self.image
        # Set sprite position
        self.drawImage = self.image
        self.angle = 0
        self.rect = pygame.Rect(x, y, w, h)
        self.recoverAngleSpeed = 0
        
        self.airballImages = []
        for i in range(0, 5):
            self.airballImages.append(globals.get_image("assets/airball" + str(i) + ".png").convert_alpha())
            self.airballImages[i] = pygame.transform.scale(self.airballImages[i], (w, h))
        self.airballImage = self.airballImages[0]
        self.hurtImage = globals.get_image("assets/crow_hurt.png").convert_alpha()
        self.hurtImage = pygame.transform.scale(self.hurtImage, (w, h))
        if flip:
            self.hurtImage = pygame.transform.flip(self.hurtImage, True, False)
        
        self.airballWaterImage = globals.get_image("assets/airball_water.png").convert_alpha()
        self.airballWaterImage = pygame.transform.scale(self.airballWaterImage, (w, h))
        
        self.shockImages = []
        for i in range(0, 4):
            self.shockImages.append(globals.get_image("assets/electricpain" + str(i) + ".png").convert_alpha())
        self.shockCounter = 0
        self.shockChangeTimer = 0
        self.hurtTimer = 0
        self.shockedTimer = 0
        self.shockImage = self.shockImages[self.shockCounter]
        self.smashed = False
        self.fillingBall = False
        self.airballFillingHeight = 0
        
        self.particles = pygame.sprite.Group()
        self.bubbleDelay = self.rect.width
        
    def getHurt(self, amount, byElectric=False):
        self.HP -= amount
        if self.HP < 1:
            self.airballImage = self.airballImages[4]
        elif self.HP < 20:
            self.airballImage = self.airballImages[3]
        elif self.HP < 45:
            self.airballImage = self.airballImages[2]
        elif self.HP < 70:
            self.airballImage = self.airballImages[1]
        
        self.hurtTimer = 1
        self.image = self.hurtImage
        if byElectric:
            self.shockChangeTimer = 0.2
            self.shockedTimer = 1
          
    def destroySelf(self):
        self.fillingBall = True
          
    def smashBall(self):
        self.gravity = 0.025
        self.smashed = True
        # Create bubbles
        edgeSize = self.rect.width / 8
        for i in range(0, 5):
            self.particles.add(entities.ball.Bubble(
                    [self.rect.x+edgeSize, self.rect.y+edgeSize, self.rect.width-2*edgeSize, self.rect.height-2*edgeSize], 1))
        # Create glass shards
        for i in range(0, 6):
            # Top
            self.particles.add(entities.sandgrain.GlassShard(self.x + i*self.rect.width/6, self.y, -8+random.random()*16, random.random()*-12, i%3))
        for i in range(0, 6):
            # Bottom
            self.particles.add(entities.sandgrain.GlassShard(self.x + i*self.rect.width/6, self.y+self.rect.height/2, -8+random.random()*16, random.random()*-12, i%3))
        
    def update(self):
        if self.fillingBall:
            self.airballFillingHeight += 0.75
            if self.airballFillingHeight >= self.rect.height:
                self.fillingBall = False
                self.smashBall()
    
        self.drawImage = pygame.transform.rotate(self.image, self.angle)
        self.angle += -self.velocityX
        if self.angle >= 360:
            self.angle -= 360
        elif self.angle <= -360:
            self.angle += 360
            
        # Return to upright angle
        if self.velocityX == 0 and self.HP > 0:
            self.recoverAngleSpeed += 0.1
            if self.recoverAngleSpeed > 4:
                self.recoverAngleSpeed = 4
        
            if self.angle < 4.1 and self.angle > -4.1:
                self.angle = 0
            elif (self.angle > 0 and self.angle < 180) or (self.angle < -180):
                self.angle -= self.recoverAngleSpeed
            else:
                self.angle += self.recoverAngleSpeed
        else:
            self.recoverAngleSpeed = 0
        
        self.particles.update()
        if self.bubbleDelay < 0:
            # When moved by size, create bubble
            self.bubbleDelay = self.rect.width/2
            edgeSize = self.rect.width / 8
            self.particles.add(entities.ball.Bubble(
                    [self.rect.x+edgeSize, self.rect.y+edgeSize, self.rect.width-2*edgeSize, self.rect.height-2*edgeSize], 1))
        
        #self.HP -= 0.1
        x = self.x
        y = self.y
        rect = self.rect
        # Move x position
        self.prevX = x
        self.prevY = y
        x += self.velocityX
        self.bubbleDelay -= abs(self.velocityX)
        # Check if I hit an edge, if so, reverse direction
        if x < 0 or x + rect.width > globals.GAME_WIDTH:
            if x < 0:
                x = 0
            else:
                x = globals.GAME_WIDTH - rect.width
            self.velocityX *= -0.3
            
        # Ground friction
        if self.standing:
            self.velocityX *= 0.96
            if abs(self.velocityX) < 0.020:
                self.velocityX = 0
        
        # Gravity still pulling when on the ground
        self.velocityY += self.gravity
        y += self.velocityY
        self.bubbleDelay -= abs(self.velocityY)
        # Move y position if in the air
        self.standing = False
        # Check if I have landed
        if y + rect.height >= globals.GAME_HEIGHT - globals.SAND_HEIGHT:
            # Bounce if going fast enough, else land
            y = globals.GAME_HEIGHT - globals.SAND_HEIGHT - rect.height
            if self.velocityY < 1 and self.velocityY > 0:
                # Create sands
                self.standing = True
                self.velocityY = 0
            else:
                if self.velocityY > 1:
                    for i in range(0, 30):
                        self.particles.add(self.createSandGrain())
                self.velocityY *= -0.15
            
        # Check if I hit the ceiling, if so, reverse direction
        elif y < 0:
            self.velocityY *= -1
        
        if self.hurtTimer > 0:
            self.hurtTimer -= 0.0125
            if self.hurtTimer <= 0:
                if self.HP > 0:
                    self.image = self.normalImage
                    
        if self.shockedTimer > 0:
            self.shockedTimer -= 0.0125
            self.shockChangeTimer -= 0.2
            if self.shockChangeTimer <= 0:
                self.shockChangeTimer = 1
                self.shockCounter += 1
                if self.shockCounter > len(self.shockImages)-1:
                    self.shockCounter = 0
                self.shockImage = self.shockImages[self.shockCounter]
                
        # Update sprite position
        self.x = x
        self.y = y
        self.rect = pygame.Rect(x, y, self.rect.width, self.rect.height)
        
    def crowCollision(self, other):
        rect = self.rect
        if globals.colliderect_edges(rect, other.rect):
            # Check if I have landed on other
            #if rect.
            pass
            
    def createSandGrain(self):
        x = self.x + (random.random() * self.rect.width)
        y = globals.GAME_HEIGHT - globals.SAND_HEIGHT
        xSpeed = self.velocityX-1 + (random.random() * 2)
        ySpeed = -self.velocityY/2 - 3 + (random.random() * 6)
        
        return entities.sandgrain.SandGrain(x, y, xSpeed, ySpeed)
    
    def draw(self, screen):
        self.particles.draw(screen)
        
        rotRect = self.drawImage.get_rect()
        dx = rotRect.width - self.rect.width
        dy = rotRect.height - self.rect.height
        screen.blit(self.drawImage, [self.rect.x - dx/2, self.rect.y - dy/2])
        if self.fillingBall:
            screen.blit(self.airballWaterImage, [self.rect.x, self.rect.bottom - self.airballFillingHeight], [0, self.rect.height - self.airballFillingHeight, self.rect.width, self.airballFillingHeight])
        if not self.smashed:
            screen.blit(self.airballImage, self.rect)
        
        if self.hurtTimer > 0 and self.shockedTimer > 0:
            screen.blit(self.shockImage, (self.x, self.y))
        
    def dividerCollision(self, div):
        div = div.rect
        rect = self.rect

        # Colliderect is not good enough because it doesn't count edges:
        if globals.colliderect_edges(rect, div):
            left = self.x
            right = left + rect.width
            top = self.y
            bottom = top + rect.height
            prevLeft = self.prevX
            prevRight = prevLeft + rect.width
            prevBottom = self.prevY + rect.height
            # Check top collision
            if prevBottom <= div.top and bottom > div.top and self.velocityY >= 0:
                # Bounce off the top
                self.y = div.top - rect.height
                if self.velocityY < 1:
                    self.standing = True
                    self.velocityY = 0
                else:
                    self.velocityY *= -0.15
            else:
                # Side collision only if there is no top collision
                if prevRight < div.left and right >= div.left:
                    # Bounce off the left
                    self.x = div.left - rect.width - 1
                    self.velocityX *= -0.3
                elif prevLeft > div.right and left <= div.right:
                    # Bounce off the right
                    self.x = div.right + 1
                    self.velocityX *= -0.3
        
    def crowBlockerCollision(self, blocker):
        div = blocker.rect
        rect = self.rect
        
        if globals.colliderect_edges(rect, div):
            # There is no top or bottom collision
            # Only collide if bottom is > blocker bottom
            if rect.bottom <= div.bottom:
                if self.prevX+rect.width < div.left and self.x+rect.width >= div.left:
                    # Bounce off the left
                    self.x = div.left - rect.width - 1
                    damage = int(self.velocityX/1.5)
                    if damage < 1:
                        damage = 1
                    self.getHurt(damage, True)
                    self.velocityX *= -1
                elif self.prevX > div.right and rect.left <= div.right:
                    # Bounce off the right
                    self.x = div.right + 1
                    damage = int(abs(self.velocityX/1.5))
                    if damage < 1:
                        damage = 1
                    self.getHurt(damage, True)
                    self.velocityX *= -1
            
    def ballCollision(self, ball):
        # Help from:
        # http://www.gamasutra.com/view/feature/131424/pool_hall_lessons_fast_accurate_.php?page=3

        rect = self.rect
        # First check if the rects touch
        if globals.colliderect_edges(rect, ball.rect):
            distance = (rect.center[0] - ball.rect.center[0]) * (rect.center[0] - ball.rect.center[0]) + \
                       (rect.center[1] - ball.rect.center[1]) * (rect.center[1] - ball.rect.center[1])
        
            if distance <= (rect.width/2 + ball.rect.width/2)*(rect.width/2 + ball.rect.width/2):
                # There is a collision
                
                n = ( (rect.center[0] - ball.rect.center[0]), (rect.center[1] - ball.rect.center[1]) )
                magnitude = math.sqrt(distance)
                # Normalise n
                n = (n[0]/magnitude, n[1]/magnitude)

                v1 = (self.velocityX, self.velocityY)
                v2 = (ball.velocityX, ball.velocityY)

                a1 = (v1[0] * n[0]) + (v1[1] * n[1])
                a2 = (v2[0] * n[0]) + (v2[1] * n[1])
                
                if self.HP <= 0:
                    circle1_mass = 0.75
                else:
                    circle1_mass = 1.25
                circle2_mass = 1

                optimizedP = (2.0 * (a1 - a2)) / (circle1_mass + circle2_mass)

                v1f = (v1[0] - optimizedP * circle2_mass * n[0]), (v1[1] - optimizedP * circle2_mass * n[1])
                v2f = (v2[0] + optimizedP * circle1_mass * n[0]), (v2[1] + optimizedP * circle1_mass * n[1])

                self.velocityX = v1f[0]
                self.velocityY = v1f[1]
                if ball.hit:
                    #ow, Fell over
                    ball.destroy()
                    self.getHurt(int(10 + ball.rect.width/10))
                ball.setSpeed(v2f[0], v2f[1])
                self.x += self.velocityX
                self.y += self.velocityY
        
    def fire(self, power, angle):
        angle -= 2*math.pi
        angle = -angle
        self.standing = False
        self.velocityX = math.cos(angle) * power*30
        self.velocityY = -math.sin(angle) * power*30
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        