import globals, pygame, math, random, entities.sandgrain

class Ball(pygame.sprite.Sprite):
    def __init__(self, x, y, size):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.prevX = x
        self.prevY = y
        self.velocityX = 0
        self.velocityY = 0
        self.gravity = 0.3
        
        self.image = globals.get_image("assets/mine.png").convert_alpha()
        original_size = self.image.get_rect().size
        self.image = pygame.transform.scale(self.image, (size, size))
        self.rect = pygame.Rect(x, y, size, size)
        
        self.hit = False
        self.smash = False
        self.particles = pygame.sprite.Group()
        self.bubbleDelay = size
        self.createBubbles = False
        self.target = None
        
    def setTarget(self, target):
        self.target = target
        self.hit = True
        
    def update(self):
        self.particles.update()
        if self.createBubbles or self.hit:
            if not self.hit:
                self.bubbleDelay -= self.rect.width/5
            if self.bubbleDelay < 0:
                # When moved by size, create bubble
                self.bubbleDelay = self.rect.width/2
                edgeSize = self.rect.width / 8
                self.particles.add(Bubble([self.rect.x+edgeSize, self.rect.y+edgeSize, self.rect.width-2*edgeSize, self.rect.height-2*edgeSize]))
    
        if not self.hit or self.smash:
            return
            
        if self.target is not None:
            # Home in on target
            otherRect = self.target.rect
            dx = otherRect.center[0] - self.rect.center[0]
            dy = otherRect.center[1] - self.rect.center[1]
            hyp = math.sqrt(dx*dx + dy*dy)
            self.velocityX = dx/hyp * 25
            self.velocityY = dy/hyp * 25
            
        self.prevX = self.x
        self.prevY = self.y
        self.x += self.velocityX
        self.bubbleDelay -= abs(self.velocityX)
        
        # Check outer wall collision
        if self.x < 0 or self.x + self.rect.width > globals.GAME_WIDTH:
            if self.x < 0:
                self.x = 0
            else:
                self.x = globals.GAME_WIDTH - self.rect.width
            self.velocityX *= -0.75
        
        if self.target is None:
            self.velocityY += self.gravity
        self.y += self.velocityY
        self.bubbleDelay -= abs(self.velocityY)
        
        if self.y + self.rect.height >= globals.GAME_HEIGHT - globals.SAND_HEIGHT:
            for i in range(0, 90):
                self.particles.add(self.createSandGrain())
            self.destroy()
        elif self.y < 0 and self.target is None:
            #self.destroy()
            self.y = 0
            self.velocityY *= -1
        
        self.rect = pygame.Rect(self.x, self.y, self.rect.width, self.rect.height)
    
    def destroy(self):
        self.smash = True
        edgeSize = self.rect.width / 8
        for i in range(0, 40):
            extra = 32
            self.particles.add(Bubble([self.rect.x-extra, self.rect.y-extra, self.rect.width+2*extra, self.rect.height+2*extra], 1, -3))
    
    def draw(self, screen):
        # Draw particles underneath me
        self.particles.draw(screen)
        screen.blit(self.image, self.rect)
        
    def createSandGrain(self):
        x = self.x + (random.random() * self.rect.width)
        y = globals.GAME_HEIGHT - globals.SAND_HEIGHT
        xSpeed = -1 + (random.random() * 2)
        ySpeed = -(self.rect.width/5) + (random.random() * (self.rect.width/5))
        
        return entities.sandgrain.SandGrain(x, y, xSpeed, ySpeed)
    
    def setCreateBubbles(self, affirmative):
        self.createBubbles = affirmative
    
    def dividerCollision(self, div):
        div = div.rect
        circle = self.rect
        if globals.colliderect_edges(circle, div):
            # Check circle against closest point on rectangle
            deltaX = circle.center[0] - max(div.x, min(circle.center[0], div.right));
            deltaY = circle.center[1] - max(div.y, min(circle.center[1], div.bottom));
            radius = circle.width/2
            if deltaX*deltaX + deltaY*deltaY <= radius*radius:
                # Bounce off the top
                bounceAllowance = 8
                if self.prevY + circle.height < div.top + circle.height/bounceAllowance:
                    # This is a crappy fix to a rare bug where the mine keeps bouncing on the corner
                    if abs(self.velocityX) <= 0.1:
                        self.destroy()
                    self.y = div.top - circle.height
                    self.velocityY *= -0.3
                    if self.velocityY > -1:
                        self.velocityY = -1
                else:
                    # Corner bounce
                    
                    if self.prevY + circle.height/2 < div.top and self.velocityY > 0:
                        if abs(self.velocityX) <= 0.1:
                            self.destroy()
                        self.y = div.top - circle.height
                        self.velocityY *= -0.5
                    
                    # Side collision only if there is no top collision
                    if self.prevX + radius < div.left and circle.right >= div.left:
                        # Bounce off the left
                        self.velocityX *= -0.3
                        self.x = self.prevX
                    elif self.prevX + radius > div.right and circle.left <= div.right:
                        # Bounce off the right
                        self.velocityX *= -0.3
                        self.x = self.prevX
                        
        self.rect = pygame.Rect(self.x, self.y, self.rect.width, self.rect.height)
        
    def ballCollision(self, ball):
        rect = self.rect
        # First check if the rects touch
        if globals.colliderect_edges(rect, ball.rect):
            distance = (rect.center[0] - ball.rect.center[0]) * (rect.center[0] - ball.rect.center[0]) + \
                       (rect.center[1] - ball.rect.center[1]) * (rect.center[1] - ball.rect.center[1])
        
            if distance <= (rect.width/2 + ball.rect.width/2)*(rect.width/2 + ball.rect.width/2):
                # There is a collision
                
                n = ( (rect.center[0] - ball.rect.center[0]), (rect.center[1] - ball.rect.center[1]) )
                magnitude = math.sqrt(distance)
                if magnitude == 0:
                    return
                # Normalise n
                n = (n[0]/magnitude, n[1]/magnitude)

                v1 = (self.velocityX, self.velocityY)
                v2 = (ball.velocityX, ball.velocityY)

                a1 = (v1[0] * n[0]) + (v1[1] * n[1])
                a2 = (v2[0] * n[0]) + (v2[1] * n[1])
                
                circle1_mass = 1
                circle2_mass = 1

                optimizedP = (2.0 * (a1 - a2)) / (circle1_mass + circle2_mass)

                v1f = (v1[0] - optimizedP * circle2_mass * n[0]), (v1[1] - optimizedP * circle2_mass * n[1])
                v2f = (v2[0] + optimizedP * circle1_mass * n[0]), (v2[1] + optimizedP * circle1_mass * n[1])
                
                self.setSpeed(v1f[0], v1f[1])
                ball.setSpeed(v2f[0], v2f[1])
            
    def setSpeed(self, xVel, yVel):
        self.hit = True
        self.velocityX = xVel
        self.velocityY = yVel
        self.x += self.velocityX
        self.y += self.velocityY
        
class Bubble(pygame.sprite.Sprite):
    def __init__(self, spawnRect, type=0, ySpeed=-0.75):
        pygame.sprite.Sprite.__init__(self)
        # Must spawn somewhere within the rect
        self.x = spawnRect[0] + (random.random() * spawnRect[2])
        self.y = spawnRect[1] + (random.random() * spawnRect[3])
        #self.velocityX = 0
        self.velocityY = ySpeed
        #self.gravity = 0.3
        self.image = globals.get_image("assets/bubble" + str(type) + ".png").convert_alpha()
        randomSize = random.randint(16, 32)
        self.image = pygame.transform.scale(self.image, (randomSize, randomSize))
        self.rect = self.image.get_rect()
        self.rect.move_ip(self.x, self.y)
        self.lifeLeft = 1 + random.random() * 5
        
    def update(self):
        self.y += self.velocityY
    
        if self.lifeLeft > 0:
            self.lifeLeft -= 0.05
        else:
            self.kill()
            
        self.rect.y = self.y
        
class BallTube(object):
    # Places balls and then disappears and removes them from list
    def __init__(self):
        self.balls = []
        self.finished = False
        self.active = False
        self.inPosition = False
        self.timer = 0
        self.imageTop = globals.get_image("assets/tube_top.png").convert_alpha()
        self.imageBottom = globals.get_image("assets/tube_bottom.png").convert_alpha()
        self.rect = self.imageTop.get_rect()
        self.rect.move_ip(globals.GAME_WIDTH/2-self.rect.width/2, -self.rect.height)
        
    def startPlaceBalls(self, balls, stayOut=False):
        self.inPosition = False
        self.active = True
        self.finished = False
        self.stayOut = stayOut
        self.timer = 0
        self.ballDict = {}
        self.activeBalls = {}
        # Map balls to their final positions and set them to their "fake" positions. Third value is used later
        if balls is not None:
            for ball in balls:
                self.ballDict[ball] = [ball.rect.x, ball.rect.y, 0.0]
                ball.rect.x = self.rect.center[0]-ball.rect.width/2
                ball.rect.y = -ball.rect.height
            #ball.velocityX = 0
            #ball.velocityY = 20
        
    def update(self):
        if self.active:
            if not self.inPosition:
                if len(self.ballDict) == 0 and not self.stayOut:
                    if self.rect.bottom > 0:
                        self.rect.y -= 6
                    else:
                        self.finished = True
                        self.active = False
                else:
                    # Move tube on screen
                    if self.rect.y < -1:
                        self.rect.y += 2
                    elif not self.stayOut:
                        self.inPosition = True
            else:
                # Add ball to dictionary if timer has run out
                if len(self.ballDict) > 0:
                    if self.timer <= 0:
                        self.timer = 1
                        ball, dest = self.ballDict.popitem()
                        self.activeBalls[ball] = dest
                        ball.setCreateBubbles(True)
                    else:
                        self.timer -= 0.05
            
                removeBalls = []
                for ball, dest in self.activeBalls.iteritems():
                    # Check if ball has finished
                    if abs(ball.rect.x - dest[0]) < 5 and abs(ball.rect.y - dest[1]) < 5:
                        removeBalls.append(ball)
                    # Move ball
                    startX = self.rect.center[0]-ball.rect.width/2;
                    startY = -ball.rect.height;
                    endX = dest[0];
                    endY = dest[1];

                    if endX > startX:
                        # Right
                        bezierX = self.rect.center[0]-ball.rect.width/2 + 128;
                    else:
                        # Left
                        bezierX = self.rect.center[0]-ball.rect.width/2 - 128;
                    bezierY = globals.GAME_HEIGHT-64;
                    
                    dest[2] += 0.02 * (max(1-dest[2], 0.5))
                    t = dest[2]
                    ball.rect.x = ((1-t)*(1-t)*startX + 2*(1-t)*t*bezierX+t*t*endX)
                    ball.rect.y = ((1-t)*(1-t)*startY + 2*(1-t)*t*bezierY+t*t*endY)
                    
                    # Catch if it went too far
                    if ball.rect.y < dest[1] and t > 0.8:
                        ball.rect.x = dest[0]
                        ball.rect.y = dest[1]
                    
                # Remove finished balls
                for ball in removeBalls:
                    ball.velocityX = 0
                    ball.velocityY = 0
                    ball.rect.x = self.activeBalls[ball][0]
                    ball.rect.y = self.activeBalls[ball][1]
                    ball.setCreateBubbles(False)
                    del self.activeBalls[ball]
                # Check if any balls left
                if len(self.ballDict) == 0 and len(self.activeBalls) == 0:
                    self.inPosition = False
        
    def drawTop(self, screen):
        screen.blit(self.imageTop, self.rect)
        
    def drawBottom(self, screen):
        screen.blit(self.imageBottom, [self.rect.x, self.rect.y+36])