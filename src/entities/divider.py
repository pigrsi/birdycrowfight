import globals, pygame

class Divider(pygame.sprite.Sprite):
    def __init__(self, x, y, w, h):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        
        self.image = pygame.Surface([w, h])
        self.image.fill(pygame.Color(0,0,0,1))
        
        # Set sprite position
        self.rect = pygame.Rect(self.x, self.y, w, h)
        
# A divider that only crows collide with
class CrowBlocker(pygame.sprite.Sprite):
    def __init__(self, x, y, w, h):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        
        self.images = []
        for i in range(0, 4):
            self.images.append(globals.get_image("assets/electric" + str(i) + ".png").convert_alpha())
        
        self.image = pygame.Surface((0,0))
        self.imageTimer = 1
        self.currentImage = 0
        
        # Set sprite position
        self.rect = pygame.Rect(self.x, self.y, w, h)
        
    def update(self):
        self.imageTimer -= 0.15
        if self.imageTimer <= 0:
            self.imageTimer = 1
            self.currentImage += 1
            if self.currentImage >= len(self.images):
                self.currentImage = 0
            self.image = self.images[self.currentImage]