import pygame
import globals

# To show when the game is waiting for something...
class Spinner(object):
    def __init__(self, x, y):
        self.image = globals.get_image("assets/spinny.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.move_ip(x, y)
        self.angle = 0.0
        
    def draw(self, screen):
        rotated = pygame.transform.rotozoom(self.image, self.angle, 1)
        rotRect = rotated.get_rect()
        dx = rotRect.width - self.rect.width
        dy = rotRect.height - self.rect.height
        screen.blit(rotated, [self.rect.x - dx/2, self.rect.y - dy/2])
        self.angle -= 10
    
    def moveTo(self, x, y):
        self.rect.x = x
        self.rect.y = y