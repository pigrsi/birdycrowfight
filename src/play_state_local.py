import pygame, math, sys, random
import event_manager, entities.crow, globals, random, network, entities.aim_arrow, entities.divider, entities.spinny
from entities.crow import Crow
from entities.aim_arrow import AimArrow
from entities.divider import Divider, CrowBlocker
from entities.ball import Ball, BallTube
from input_controller import ButtonManager, Button, createButton
from play_state import HUD, GameOverBox

class PlayStateLocal(object):
    def __init__(self, main_screen, eventManager):
        self.game_screen = pygame.Surface((globals.GAME_WIDTH, globals.GAME_HEIGHT))
        self.main_screen = main_screen
        self.eventManager = eventManager
        self.eventManager.RegisterListener(self)
        self.buttonManager = ButtonManager(globals.GAME_WIDTH, globals.GAME_HEIGHT)
        self.HUD = HUD(self.main_screen)
        self.isConnected = False
        
        self.gameModel = LocalGameModel(self, self.game_screen, self.buttonManager)
        
        self.gameOverBox = None
            
        # Text font
        font = pygame.font.SysFont("tahoma", 28)
        
        # Return to menu button
        buttonFont = pygame.font.SysFont("calibri", 16)
        self.returnButton = createButton(buttonFont, "Return to Menu", None, self.returnToMenu)
        self.returnButton.rect = pygame.Rect(globals.GAME_WIDTH-169, 8, 161, 42)
        self.buttonManager.add(self.returnButton)
        
        # Background
        self.backgroundImage = globals.get_image("assets/background.png").convert_alpha()
        
    def update(self, delta):
        self.main_screen.fill((255,228,181))
        self.main_screen.fill((150,175,250), [globals.game_screen_offset, 0, globals.GAME_WIDTH, globals.GAME_HEIGHT])
        self.game_screen.fill((150,175,250))
        self.buttonManager.update()
        
        if self.gameOverBox is not None:
            self.gameOverBox.update()
        
        self.gameModel.update(delta)
        
    def draw(self):
        self.game_screen.blit(self.backgroundImage, (0,0))
        self.gameModel.draw()
        self.buttonManager.draw(self.game_screen)
       
        if self.gameOverBox is not None:
            self.gameOverBox.draw(self.game_screen)
            
    def drawHUD(self, leftHP, rightHP, timeRemaining):
        self.HUD.draw(leftHP, rightHP, timeRemaining)
            
    def getSecondScreen(self):
        return self.game_screen

    def HandleInput(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP:
            if self.gameOverBox:
                self.gameOverBox.HandleInput(event)
            else:
                self.buttonManager.checkPressed(event)
            
        elif event.type == pygame.KEYUP or event.type == pygame.KEYDOWN:
            self.gameModel.keyEvent(event)    
            
    def gameIsOver(self, gameOverCode):
        if not self.gameOverBox:
            self.createGameOverBox(gameOverCode)
            #self.gameOverBox.addButton(self.returnButton)
            
    def createGameOverBox(self, boxCode):
        buttonFont = pygame.font.SysFont("calibri", 16)
        header = globals.get_image("assets/game_over.png").convert_alpha()
        if boxCode == 0:
            # Left wins
            middle = globals.get_image("assets/left_wins.png").convert_alpha()
            pass
        elif boxCode == 1:
            # Right wins
            middle = globals.get_image("assets/right_wins.png").convert_alpha()
            pass
        elif boxCode == 2:
            # Tie
            middle = globals.get_image("assets/you_tied.png").convert_alpha()
            
        button = createButton(buttonFont, "Rematch!", None, self.startNewGame)
            
        self.gameOverBox = GameOverBox(header, middle)
        box = self.gameOverBox.boxRect
        button.rect.move_ip(box.center[0]-button.rect.width/2, box.y+0.75*box.height)
        self.gameOverBox.addButton(button)
            
    def returnToMenu(self):
        self.eventManager.Post(event_manager.ChangeStateEvent("MenuState"))
        pygame.display.set_caption("Birdy Crowfight")
        globals.screen_shake_offset = [0, 0]
            
    def startNewGame(self):
        self.buttonManager.clear()
        self.buttonManager.add(self.returnButton)
        self.gameOverBox = None
        self.gameModel = LocalGameModel(self, self.game_screen, self.buttonManager)
        
    def Notify(self, event):            
        self.gameModel.Notify(event)
        
class LocalGameModel(object):
    def __init__(self, playState, screen, buttonManager):
        # From common model
        self.STATE_CREATING = 0
        self.STATE_TAKE_THE_SHOT = 1
        self.STATE_GAME_OVER = 2
        self.state = self.STATE_CREATING
        
        self.playState = playState
        self.screen = screen
        self.buttonManager = buttonManager
        
        self.leftCrow = Crow(64, globals.GAME_HEIGHT-72-globals.SAND_HEIGHT, 72, 72)
        self.rightCrow = Crow(globals.GAME_WIDTH-128, globals.GAME_HEIGHT-72-globals.SAND_HEIGHT, 72, 72, True)
        self.crowGroup = pygame.sprite.Group(self.leftCrow, self.rightCrow)
        
        self.divider = Divider(globals.GAME_WIDTH/2-12, 712, 24, globals.GAME_HEIGHT-712)
        self.crowBlocker = CrowBlocker(self.divider.x, 0, self.divider.rect.width, globals.GAME_HEIGHT-globals.SAND_HEIGHT-self.divider.rect.height)
        self.dividerGroup = pygame.sprite.Group(self.divider, self.crowBlocker)

        self.shot = None
        self.shotTaken = False
        
        self.game_is_over = False
        
        self.shaking = False
        self.gameOverAssaultTimer = 10
        self.lastTarget = None
        self.waitingForBalls = False
        
        self.ballTube = BallTube()
        self.orphanParticles = pygame.sprite.Group()
        
        random.seed()
        
        # From server model
        self.ballGroup = pygame.sprite.Group()
        self.ballGroup.add(self.generateBalls([64, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 312], 1, 1))
        self.ballGroup.add(self.generateBalls([globals.GAME_WIDTH/2+12, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 312], 1, 1))
        self.ballTube.startPlaceBalls(self.ballGroup)
               
        self.aimArrowLeft = AimArrow(self.leftCrow, self.screen, buttonManager)
        self.aimArrowRight = AimArrow(self.rightCrow, self.screen, buttonManager)
        
        if random.randint(0, 1) == 0:
            self.leftGoesNext = True
        else:
            self.leftGoesNext = False
        
    def startShake(self):
        self.shaking = True
        self.shakeTimer = 0.25
        
    def update(self, delta):
        if self.state == self.STATE_TAKE_THE_SHOT:
            self.TakeTheShot(delta)
        elif self.state == self.STATE_GAME_OVER:
            self.GameOverState()
        elif self.state == self.STATE_CREATING:
            self.Creating()
    
        self.orphanParticles.update()
        self.crowGroup.update()
        self.ballGroup.update()
        self.dividerGroup.update()
        if self.leftGoesNext and not self.aimArrowLeft.fired:
            self.aimArrowLeft.update(self, self.screen, delta)
        elif not self.leftGoesNext and not self.aimArrowRight.fired:
            self.aimArrowRight.update(self, self.screen, delta)
            
        if self.shaking:
            if self.shakeTimer > 0:
                self.shakeTimer -= delta
                globals.screen_shake_offset = [random.randint(-8, 8), random.randint(-8, 8)]
            else:
                self.shaking = False
                globals.screen_shake_offset = [0, 0]
            
        # No collision when balls are spawning
        if self.state is self.STATE_GAME_OVER:
            self.ballTube.update()
            self.checkCollision()
        else:
            if self.ballTube.active and not self.ballTube.finished:
                self.ballTube.update()
            else:
                self.checkCollision()
        # Check if any balls need to be removed
        toBeRemoved = []
        for ball in self.ballGroup.sprites():
            if ball.smash:
                for bubble in ball.particles:
                    self.orphanParticles.add(bubble)
                    self.startShake()
                toBeRemoved.append(ball)
        for ball in toBeRemoved:
            self.ballGroup.remove(ball)
        
    def Creating(self):
        if not self.ballTube.finished:
            return
        else:
            self.changeState(self.STATE_TAKE_THE_SHOT)
            if self.leftGoesNext:
                self.aimArrowLeft.reset()
            else:
                self.aimArrowRight.reset()
        
    def TakeTheShot(self, delta):
        shotTaker = None
        currentArrow = None
        if self.leftGoesNext:
            shotTaker = self.leftCrow
            currentArrow = self.aimArrowLeft
        else:
            shotTaker = self.rightCrow
            currentArrow = self.aimArrowRight

        if self.shot is not None:
            self.fireCrow(shotTaker)
            self.shot = None
            
        # Turn is over.
        if not self.shot and currentArrow.fired and self.shotTaken and self.allMovementFinished():
            # Check if the game is over. If not, go to next turn
            if self.leftCrow.HP <= 0 or self.rightCrow.HP <= 0:
                self.changeState(self.STATE_GAME_OVER)
                self.playState.gameIsOver(self.getGameOverCode())
            else:
                if not self.waitingForBalls:
                    self.createNewBalls()
                    self.waitingForBalls = True
                elif self.ballTube.finished:
                    self.leftGoesNext = not self.leftGoesNext
                    if self.leftGoesNext:
                        self.aimArrowLeft.reset()
                    else:
                        self.aimArrowRight.reset()
                    self.waitingForBalls = False
                    self.shotTaken = False

    def getGameOverCode(self):
        if self.leftCrow.HP <= 0 and self.rightCrow.HP <= 0:
            return 2
        elif self.leftCrow.HP <= 0:
            return 1
        else:
            return 0
    
    def createNewBalls(self):
        numLeft = numRight = 0
        for ball in self.ballGroup.sprites():
            if ball.rect.center[0] < globals.GAME_WIDTH/2:
                numLeft += 1
            else:
                numRight += 1
                
        newBalls = []
        # Don't create new balls if game is over
        if self.leftCrow.HP > 0 and self.rightCrow.HP > 0:
            if self.leftGoesNext:
                if numLeft < 2:
                    newBalls = newBalls + self.generateBalls([64, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 312], 2, 2)
                if numRight < 2:
                    newBalls = newBalls + self.generateBalls([globals.GAME_WIDTH/2+12, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 312], 2, 2)
            else:
                if numLeft < 2:
                    newBalls = newBalls + self.generateBalls([64, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 312], 2, 2)
                if numRight < 2:
                    newBalls = newBalls + self.generateBalls([globals.GAME_WIDTH/2+12, 160, globals.GAME_WIDTH/2-12-64, globals.GAME_HEIGHT - 192 - 312], 2, 2)

        if self.leftCrow.HP > 0 and self.rightCrow.HP > 0:
            self.ballGroup.add(newBalls)
            self.ballTube.startPlaceBalls(newBalls)
                
    def draw(self):
        self.playState.drawHUD(self.leftCrow.HP, self.rightCrow.HP, 20)
    
        if self.ballTube.active and not self.ballTube.finished:
            self.ballTube.drawBottom(self.screen)
    
        self.dividerGroup.draw(self.screen)
    
        for ball in self.ballGroup:
            ball.draw(self.screen)
            
        if self.ballTube.active and not self.ballTube.finished:
            self.ballTube.drawTop(self.screen)
            
        for crow in self.crowGroup:
            crow.draw(self.screen)

        if self.leftGoesNext and not self.aimArrowLeft.fired:
            self.aimArrowLeft.draw(self.playState.main_screen, (globals.WINDOW_WIDTH-globals.GAME_WIDTH)/2)
            self.aimArrowLeft.draw(self.screen)
        elif not self.leftGoesNext and not self.aimArrowRight.fired:
            self.aimArrowRight.draw(self.playState.main_screen, (globals.WINDOW_WIDTH-globals.GAME_WIDTH)/2)
            self.aimArrowRight.draw(self.screen)
                
        self.orphanParticles.draw(self.screen)
        
    def checkCollision(self):
        # Crow collision with divider
        if self.divider.y < globals.GAME_HEIGHT:
            self.leftCrow.dividerCollision(self.divider)
            self.rightCrow.dividerCollision(self.divider)
        # Crow collision with blocker
        self.leftCrow.crowBlockerCollision(self.crowBlocker)
        self.rightCrow.crowBlockerCollision(self.crowBlocker)
        # Crow, divider collision with balls
        for ball in self.ballGroup.sprites():
            self.leftCrow.ballCollision(ball)
            self.rightCrow.ballCollision(ball)
            ball.dividerCollision(self.divider)
        
        # Ball collision with balls. Don't do same collision twice
        if not self.state is self.STATE_GAME_OVER:
            uncheckedBalls = self.ballGroup.sprites()
            for ball in uncheckedBalls:
                uncheckedBalls = uncheckedBalls[1:]
                for otherBall in uncheckedBalls:
                    if ball.hit or otherBall.hit:
                        ball.ballCollision(otherBall)
                        
    def allMovementFinished(self):
        # When everything stops moving
        if self.leftCrow.velocityX is not 0 or not self.leftCrow.standing:
            return False
        elif self.rightCrow.velocityX is not 0 or not self.rightCrow.standing:
            return False
        
        for ball in self.ballGroup.sprites():
            if ball.velocityX is not 0 or ball.velocityY is not 0:
                return False
                
        return True
        
    def setShot(self, shot_id, power, angle, arrow):
        self.shot = (power, angle)
        if arrow is not None:
            arrow.fired = True
            
    def fireCrow(self, crow):
        crow.fire(self.shot[0], self.shot[1])
        self.shotTaken = True
        
    def changeState(self, state):
        self.state = state
        
    def GameOverState(self):
        if self.leftCrow.HP <= 0:
            if not self.leftCrow.smashed:
                self.leftCrow.destroySelf()
        if self.rightCrow.HP <= 0:
            if not self.rightCrow.smashed:
                self.rightCrow.destroySelf()
                
        if self.gameOverAssaultTimer > 0:
            self.gameOverAssaultTimer -= 0.05
        else:
            if len(self.ballGroup.sprites()) == 0:
                # Make more
                if not self.ballTube.active:
                    self.ballTube.startPlaceBalls(None, True)
                    self.gameOverAssaultTimer = 1
                else:
                    ball = Ball(self.ballTube.rect.center[0]-36, -36, 72)
                    self.ballGroup.add(ball)
            else:
                self.gameOverAssaultTimer = 1
                for ball in self.ballGroup.sprites():
                    if ball.target is not None:
                        continue
                    # Initial target
                    if self.lastTarget is None:
                        if self.leftCrow.HP <= 0:
                            ball.setTarget(self.leftCrow)
                        else:
                            ball.setTarget(self.rightCrow)
                            
                    else:
                        # If they both died, swap between targets. Otherwise keep initial target
                        if self.leftCrow.HP <= 0 and self.rightCrow.HP <= 0:
                            if self.lastTarget is self.leftCrow:
                                ball.setTarget(self.rightCrow)
                            else:
                                ball.setTarget(self.leftCrow)
                    self.lastTarget = ball.target
                    break
                    
    def keyEvent(self, event):
        if self.leftGoesNext:
            self.aimArrowLeft.keyEvent(event) 
        else:
            self.aimArrowRight.keyEvent(event)
        
    def generateBalls(self, bounds, min, max):
        # How many balls to make
        balls = []
        num_balls = random.randint(min, max)
        
        i = 0
        while i < num_balls:
            # Random size
            size = random.randint(64, 96)
            ball_x = bounds[0] + (random.random() * (bounds[2] - size))
            ball_y = bounds[1] + (random.random() * (bounds[3] - size))
            # Make sure there is not already a ball there
            spaceTaken = False
            for ball in self.ballGroup.sprites() + balls:
                if ball.rect.colliderect(pygame.Rect(ball_x, ball_y, size, size)):
                    spaceTaken = True
                    break;
            if not spaceTaken:
                ball = Ball(ball_x, ball_y, size)
                balls.append(ball)
                i += 1
            
        return balls
        
    def Notify(self, event):
        pass











