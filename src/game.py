import sys, pygame
from twisted.internet import task, reactor
from twisted import spread
import twisted.python.log
# My stuff
import event_manager, input_controller, globals, network, menu_state, play_state, play_state_local

def main():    
    pygame.display.set_caption("Birdy Crowfight")
    pygame.display.set_icon(globals.get_image("assets/icon.ico").convert_alpha())
    spinner = CPUSpinnerController(eventManager)
    gameUpdate = GameUpdate(eventManager)
    gameDraw = GameDraw()
    input = input_controller.InputController(eventManager)
    twisted.python.log.startLogging(sys.stdout)
    
    eventManager.RegisterListener(spinner)
    eventManager.RegisterListener(gameUpdate)
    eventManager.RegisterListener(gameDraw)
    eventManager.RegisterListener(input)
    
    globals.currentState = menuState
    
    loopingCall = task.LoopingCall(FireTick, eventManager)
    loopingCall.start(1.0 / globals.FPS)
    reactor.run()
    # spinner.Run()

def FireTick(eventManager):
    eventManager.Post(event_manager.TickEvent())
    _clock.tick(60)
    
class GameUpdate(object):
    def __init__(self, eventManager):
        self.eventManager = eventManager
    def Notify(self, event):
        if isinstance(event, event_manager.TickEvent):
            self.update()
        elif isinstance(event, event_manager.ChangeStateEvent):
            if event.newState == "PlayState":
                globals.currentState = play_state.PlayState(main_screen, eventManager, event.isClient)
                createNetworkObjects(event.isClient)
            elif event.newState == "PlayStateLocal":
                globals.currentState = play_state_local.PlayStateLocal(main_screen, eventManager)
            elif event.newState == "MenuState":
                globals.currentState = menuState
            
    def update(self):
        globals.currentState.update(_clock.get_time() / 1000.0)
        
def createNetworkObjects(isClient):
    globals.IS_SERVER = not isClient
    if not isClient:
        if globals.ncv is None or globals.ncc is None:
            globals.ncv = network.NetworkClientView(eventManager)
            globals.ncc = network.NetworkClientController(eventManager)
        globals.ncc.Listen(reactor)
    else:
        if globals.nsv is None or globals.nsc is None:
            globals.nsv = network.NetworkServerView(eventManager, reactor)
            globals.nsc = network.NetworkServerController(eventManager)
        globals.nsv.StartAttempting()

        
class GameDraw(object):
    def Notify(self, event):
        if isinstance(event, event_manager.TickEvent):
            globals.currentState.draw()
            if isinstance(globals.currentState, menu_state.MenuState):
                # Menu state
                main_screen.blit(globals.currentState.getSecondScreen(), [globals.menu_screen_offset[0], globals.menu_screen_offset[1]])
            else:
                # Game state
                main_screen.blit(globals.currentState.getSecondScreen(), [globals.game_screen_offset+globals.screen_shake_offset[0], globals.screen_shake_offset[1]])
            pygame.display.update()
   
class CPUSpinnerController(object):
    def __init__(self, eventManager):
        self.eventManager = eventManager
        
    # Currently unused - because the twisted reactor posts the tick event instead
    #def Run(self):
    #    while self.keepGoing:
    #        self.eventManager.Post(event_manager.TickEvent())
    #        _clock.tick(60)
    
    def Notify(self, event):
        if isinstance(event, event_manager.QuitEvent):
            reactor.stop()
            
pygame.init()
globals.DEBUG_FONT = pygame.font.SysFont("tahoma", 30)
_clock = pygame.time.Clock()
_clock.tick(60) # Calling this once here so delta is not initially huge
# Game screen will be displayed on top of the Main screen
eventManager = event_manager.EventManager()
main_screen = pygame.display.set_mode((globals.WINDOW_WIDTH, globals.WINDOW_HEIGHT))
menuState = menu_state.MenuState(main_screen, eventManager)
#menu_screen = pygame.Surface((globals.MENU_WIDTH, globals.MENU_HEIGHT))
#game_screen = pygame.Surface((globals.GAME_WIDTH, globals.GAME_HEIGHT))
main()