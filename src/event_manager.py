import sys
class EventManager(object):
    def __init__(self):
        # weakref so listener's don't have to unregister
        from weakref import WeakKeyDictionary
        self.listeners = WeakKeyDictionary()
        
    def RegisterListener(self, listener):
        self.listeners[listener] = 1
        
    def UnregisterListener(self, listener):
        del self.listeners[listener]
        
    # Broadcast event to all listeners
    def Post(self, event):  
        #print(event.name)
        sys.stdout.flush()
        for listener in self.listeners.keys():
            listener.Notify(event)

class Event(object):
    def __init__(self):
        self.name = "Generic Event"
        
class TickEvent(Event):
    def __init__(self):
        self.name = "Tick Event"
        
class QuitEvent(Event):
    def __init__(self):
        self.name = "Quit Event"
        
class ChangeStateEvent(Event):
    def __init__(self, newState, isClient=False):
        self.name = "Change state Event"
        self.newState = newState
        self.isClient = isClient

class ConnectionErrorEvent(Event):
    def __init__(self, failure):
        self.name = "Connection failed Event"
        self.failure = failure

# Play events     
class GameOverEvent(Event):
    def __init__(self, gameEndCode):
        self.name = "Game over event"
        self.gameEndCode = gameEndCode

class ServerContinueGame(Event):
    def __init__(self):
        self.name = "Server Continue game"

class ClientContinueGame(Event):
    def __init__(self):
        self.name = "Client continue game"
        
class ServerCountdownOver(Event):
    def __init__(self):
        self.name = "Server countdown is over"
        
class ClientCountdownOver(Event):
    def __init__(self):
        self.name = "Client countdown is over"

class ServerTakingShot(Event):
    def __init__(self, shot_id, power, angle):
        self.name = "Server taking a shot"
        self.shot_id = shot_id
        self.angle = angle
        self.power = power
        
class ClientTakingShot(Event):
    def __init__(self, shot_id, power, angle):
        self.name = "Client taking a shot"
        self.shot_id = shot_id
        self.angle = angle
        self.power = power

# Tell client which side their player is on
class InformSide(Event):
    def __init__(self, clientIsLeft, yourTurn):
        self.name = "Informing side"
        self.clientIsLeft = clientIsLeft
        self.yourTurn = yourTurn
        
class SyncClientEvent(Event):
    def __init__(self, leftCrowState, rightCrowState, balls, dividerHeight):
        self.name = "Syncing client objects"
        self.leftCrowState = leftCrowState
        self.rightCrowState = rightCrowState
        self.balls = balls
        self.dividerHeight = dividerHeight
        
class ServerRematchChoice(Event):
    def __init__(self, affirmative):
        self.name = "Play again choice made"
        self.affirmative = affirmative
        
class ClientRematchChoice(Event):
    def __init__(self, affirmative):
        self.name = "Play again choice made"
        self.affirmative = affirmative
        
# Network setup events
class ServerConnectEvent(Event):
    def __init__(self, server):
        self.name = "From client: Client has connected"
        self.server = server
        
class ClientConnectEvent(Event):
    def __init__(self, client):
        self.name = "For server: Client has connected"
        self.client = client
        
class ServerStillConnected(Event):
    def __init__(self):
        self.name = "Server still connected"
        
class ClientStillConnected(Event):
    def __init__(self):
        self.name = "Client still connected"
        
class StopListening(Event):
    def __init__(self):
        self.name = "Stop listening!"
        
class Disconnect(Event):
    def __init__(self):
        self.name = "Disconnect"