from twisted import spread, internet
from twisted.spread import pb
import event_manager, globals
import pygame, sys

# Logic for client-server type deal
           
serverToClientEvents = []
clientToServerEvents = []
serverListeningPort = None

### SERVER SIDE ###
class NetworkClientView(object):
    """We SEND events to the CLIENT through this object"""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        # Only one client
        self.client = None
        
    def Notify(self, event):
        if not globals.IS_SERVER:
            return
        if isinstance(event, event_manager.ClientConnectEvent):
            serverListeningPort.stopListening()
            self.client = event.client
            #print("Client has connected to me")
            sys.stdout.flush()
            pygame.display.set_caption("Birdy Crowfight - Running as Server")
        elif isinstance(event, event_manager.StopListening):
            if serverListeningPort is not None:
                serverListeningPort.stopListening()
        elif isinstance(event, event_manager.Disconnect):
            if serverListeningPort is not None:
                serverListeningPort.loseConnection()
            self.client = None
        #don't broadcast events that aren't Copyable
        if event.__class__ not in serverToClientEvents or not isinstance(event, pb.Copyable):
            return
        
        if self.client is not None:
            #print("Server sending: " + str(event.name))
            try:
                remoteCall = self.client.callRemote("ServerEvent", event)
            except Exception, e: 
                self.evManager.Post(event_manager.ConnectionErrorEvent(e))
            #except spread.pb.DeadReferenceError: 
            #    self.evManager.Post(event_manager.ConnectionErrorEvent(spread.pb.DeadReferenceError))
 
class NetworkClientController(pb.Root):
    """ We RECEIVE events from the CLIENT through this object """
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
    
    def Listen(self, reactor):
        global serverListeningPort
        try:
            serverListeningPort = reactor.listenTCP(8337, spread.pb.PBServerFactory(self))
        except Exception, e:
            self.evManager.Post(event_manager.ConnectionErrorEvent(e))

    def remote_ClientEvent(self, event):
        self.evManager.Post(event)
        #print("Server received: " + str(event.name))

    def Notify(self, event):
        pass
        #if isinstance(event, event_manager.StopListening):
        #    if self.factory is not None:
        #        self.factory.stopFactory()

### CLIENT SIDE ###
class NetworkServerView(pb.Root):
    """We SEND events to the server through this object"""
    STATE_PREPARING = 0
    STATE_CONNECTING = 1
    STATE_CONNECTED = 2
    STATE_DISCONNECTING = 3
    STATE_DISCONNECTED = 4
    
    def __init__(self, evManager, reactor):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        self.reactor = reactor
        self.pbClientFactory = pb.PBClientFactory()
        self.state = NetworkServerView.STATE_DISCONNECTED
        self.server = None
        
    def AttemptConnection(self):
        self.connection = self.reactor.connectTCP(globals.IP_ADDRESS, 8337, self.pbClientFactory)
        deferred = self.pbClientFactory.getRootObject()
        deferred.addCallback(self.Connected)
        deferred.addErrback(self.ConnectFailed)
        # Already running reactor for main loop
        # self.reactor.run()
        
    def StartAttempting(self):
        self.state = NetworkServerView.STATE_PREPARING
        
    def Connected(self, server):
        #print("Connected successfully.")
        pygame.display.set_caption("Birdy Crowfight - Connected as Client")
        sys.stdout.flush()
        
        self.server = server
        self.state = NetworkServerView.STATE_CONNECTED
        self.evManager.Post(event_manager.ServerConnectEvent(server))
        
    def ConnectFailed(self, failure):
        #print("Connection failed.")
        #print(failure)
        sys.stdout.flush()
        self.evManager.Post(event_manager.ConnectionErrorEvent(failure))
        self.state = NetworkServerView.STATE_DISCONNECTED
        
    def Notify(self, event):
        if globals.IS_SERVER:
            return
        if isinstance(event, event_manager.TickEvent):
            if self.state == NetworkServerView.STATE_PREPARING:
                self.AttemptConnection()
        elif isinstance(event, event_manager.Disconnect):
            self.server = None
            self.connection.disconnect()

        #don't broadcast events that aren't Copyable
        if not isinstance(event, pb.Copyable) or event.__class__ not in clientToServerEvents:
            return
            
        if self.server is not None:
            #print("Client sending: " + str(event.name))
            try:
                remoteCall = self.server.callRemote("ClientEvent", event)
            except Exception, e: 
                self.evManager.Post(event_manager.ConnectionErrorEvent(e))
            #except spread.pb.DeadReferenceError: 
            #    self.evManager.Post(event_manager.ConnectionErrorEvent(spread.pb.DeadReferenceError))

class NetworkServerController(pb.Referenceable):
    """We RECEIVE events from the server through this object"""
    def __init__(self, evManager):
        self.evManager = evManager
        self.evManager.RegisterListener(self)
        
    def remote_ServerEvent(self, event):
        self.evManager.Post(event)
        #print("Client received: " + str(event.name))
       
        
    def Notify(self, event):
        if globals.IS_SERVER:
            return
        if isinstance(event, event_manager.ServerConnectEvent):
            # Tell the server we're listening to it and it can
            # access this object
            event.server.callRemote("ClientEvent", event_manager.ClientConnectEvent(self))
        
        
def MixInClass( origClass, addClass ):
	if addClass not in origClass.__bases__:
		origClass.__bases__ += (addClass,)        
        
# Mix in Copyable and RemoteCopy so these events can be sent by the server
def MixInCopyClasses( someClass ):
	MixInClass( someClass, pb.Copyable )
	MixInClass( someClass, pb.RemoteCopy )
    
def mixUnjellyAndAppend(eventClass, addToServer, addToClient):
    MixInCopyClasses(eventClass)
    pb.setUnjellyableForClass(eventClass, eventClass)
    if addToServer:
        serverToClientEvents.append(eventClass)
    if addToClient:
        clientToServerEvents.append(eventClass)
    
def populateClientToServerEvents():
    mixUnjellyAndAppend(event_manager.ClientCountdownOver, False, True)
    mixUnjellyAndAppend(event_manager.ClientTakingShot, False, True)
    mixUnjellyAndAppend(event_manager.ClientRematchChoice, False, True)
    mixUnjellyAndAppend(event_manager.ServerConnectEvent, False, True)
    mixUnjellyAndAppend(event_manager.ClientContinueGame, False, True)
    mixUnjellyAndAppend(event_manager.ClientStillConnected, False, True)
    
def populateServerToClientEvents():
    mixUnjellyAndAppend(event_manager.ServerCountdownOver, True, False)
    mixUnjellyAndAppend(event_manager.ClientConnectEvent, True, False)
    mixUnjellyAndAppend(event_manager.ServerTakingShot, True, False)
    mixUnjellyAndAppend(event_manager.SyncClientEvent, True, False)
    mixUnjellyAndAppend(event_manager.InformSide, True, False)
    mixUnjellyAndAppend(event_manager.ServerContinueGame, True, False)
    mixUnjellyAndAppend(event_manager.GameOverEvent, True, False)
    mixUnjellyAndAppend(event_manager.ServerStillConnected, True, False)
    mixUnjellyAndAppend(event_manager.ServerRematchChoice, True, False)
    
populateClientToServerEvents()
populateServerToClientEvents()
