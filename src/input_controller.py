import pygame, sys
import event_manager
import globals

class InputController(object):
    def __init__(self, eventManager):
        self.eventManager = eventManager
    def Notify(self, event):
        if isinstance(event, event_manager.TickEvent):
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    globals.currentState.HandleInput(event)
                elif event.type == pygame.MOUSEBUTTONUP:
                    globals.currentState.HandleInput(event)
                elif event.type == pygame.KEYUP:
                    globals.currentState.HandleInput(event)
                elif event.type == pygame.KEYDOWN:
                    globals.currentState.HandleInput(event)
                # Why is this in the input controller?
                elif event.type == pygame.QUIT:
                    self.eventManager.Post(event_manager.QuitEvent())
                   
# Give it a list of buttons and positions and it will call functions if a button was pressed                   
class ButtonManager(object):
    def __init__(self, screenWidth, screenHeight, buttons=None):
        if buttons is None:
            self.buttons = []
        else:
            self.buttons = buttons
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
            
    def update(self):
        # Checks if any buttons are hovered and calls their functions
        mousePos = globals.get_innerscreen_mousepos(self.screenWidth, self.screenHeight)
        for butt in self.buttons:
            if butt.rect.collidepoint(mousePos):
                butt.buttonHovered()
            else:
                butt.buttonUnhovered()
        
    def checkPressed(self, event):
        # 1 is the code for left click
        if event.button is not 1:
            return
            
        if event.type == pygame.MOUSEBUTTONUP:
            for butt in self.buttons:
                butt.buttonUnpressed()
        # Only one button can be pressed at a time
        elif event.type == pygame.MOUSEBUTTONDOWN:   
            mousePos = globals.get_innerscreen_mousepos(self.screenWidth, self.screenHeight)
            for butt in self.buttons:
                if butt.rect.collidepoint(mousePos):
                    butt.buttonPressed()
                    return
                    
    def draw(self, screen):
        for button in self.buttons:
            if not button.dontDraw:
                # nullifies screen shake
                screen.blit(button.image, [button.rect.x - globals.screen_shake_offset[0], button.rect.y - globals.screen_shake_offset[1]])
                    
    def add(self, button):
        self.buttons.append(button)
        
    def remove(self, button):
        self.buttons.remove(button)
        
    def contains(self, button):
        return button in self.buttons
        
    def clear(self):
        self.buttons = []
        
class Button(pygame.sprite.Sprite):
    def __init__(self, unpressWhenCursorLeaves, image=None, pressedAction=None, unpressedAction=None, stayOverlappedToUnpress=False):
        pygame.sprite.Sprite.__init__(self)
        self.unpressWhenCursorLeaves = unpressWhenCursorLeaves
        self.stayOverlappedToUnpress = stayOverlappedToUnpress
        
        if image == None:
            image = pygame.Surface([0, 0])
        self.unpressedImage = image
        self.hoveredImage = image
        self.pressedImage = image
        # self.image is the one that is drawn
        self.image = self.unpressedImage
        
        self.pressedAction = pressedAction
        self.unpressedAction = unpressedAction
        self.rect = self.image.get_rect()
        self.notPressed = True
        self.dontDraw = False
        
    # When the button is neither being pressed or hovered on
    def buttonUnhovered(self):
        self.hovered = False
        if self.notPressed:
            self.image = self.unpressedImage
        elif self.unpressWhenCursorLeaves:
            # If they have unhovered the button is unpressed if this is true
            self.unpressedAction()
        
    def buttonHovered(self):
        self.hovered = True
        if self.notPressed:
            self.image = self.hoveredImage
    
    def buttonPressed(self):
        self.image = self.pressedImage
        self.notPressed = False
        if self.pressedAction is not None:
            self.pressedAction()

    def buttonUnpressed(self):
        if not self.notPressed and self.unpressedAction is not None:
            if not (self.stayOverlappedToUnpress and not self.hovered):
                self.unpressedAction()
        self.notPressed = True
        
        
# Blits the text onto each template and returns the new images
# Create button images (slap the text on the button templates)
def createButton(font, text, pressedAction, unpressedAction):
    renderedTextDark = font.render(text, 1, (83, 83, 83))
    
    unpressed = globals.get_image("assets/button_unpressed.png").convert()
    hovered = globals.get_image("assets/button_hovered.png").convert()
    pressed = globals.get_image("assets/button_pressed.png").convert()
    
    # Blit the text onto the three images
    buttonRect = pressed.get_rect()
    textSize = font.size(text)
    textX = (buttonRect.width-textSize[0])/2
    textY = (buttonRect.height-textSize[1])/2 + 2
    
    unpressed.blit(renderedTextDark, [textX, textY])
    hovered.blit(renderedTextDark, [textX, textY])
    pressed.blit(renderedTextDark, [textX, textY])

    # Create the button
    newButton = Button(False, unpressed, pressedAction, unpressedAction, True)
    newButton.hoveredImage = hovered
    newButton.pressedImage = pressed
    return newButton