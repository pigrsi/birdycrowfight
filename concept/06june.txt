Add spinner while waiting for rematch choice. Can also be used when waiting for other player to make a move. Doesn't have to be  aspinny. Could be  a pulsating circle or something. But a spinny is more clear.

Spinny while other player is having their turn (in their top corner)

gameplay concerns:
Should hitting a mine into the ceiling destroy it?
Is it too hard to hit the other player?
Does hitting the other player rely too much on luck?
Should damage be based on the size of the mine?
How many mines should spawn each turn?
Is it fun?

- Hitting other player is easier with heavier crow, but bonking a ball not as satisfying.


New mines coming into place:
Initial mines are already there
Others: tube comes down from top middle:
Mines come out one at a time and move into their spots
They cannot collide with anything at this time.
Tube goes away

How to smoothly move balls into position:
this is the same for all balls:
y uses speed - ball is given strong initial y velocity downwards
applies an acceleration upwards until it is speed 0. should stop below the allowable spawn point.

not the same
upwards speed now depends on distance to finishing point.
x doesn't use speed it 




