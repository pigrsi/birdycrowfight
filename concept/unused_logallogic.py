# Logic for offline game
class LocalLogic(object):

    STATE_PREPARING = 0 #############################
    STATE_RUNNING = 1  #############################
    STATE_PAUSED = 2  #############################

    def __init__(self, playState):
        self.playState = playState;
        
        # Generate game objects
        pass #TODO###########################################
        self.players = (Player(0), Player(1))
        
        # Randomly pick who is going to start
        if random.random() < 0.5:
            self.turn = 0
        else:
            self.turn = 1
        self.gameOver = False
        # First turn, countdown is 30, afterwards it's 20
        self.countdown = 30
        
        # Temporary variables for game states
        self.shotTaken = False
        
    def update(self, delta):
        # Check if someone has died.
        if self.players[0].died or self.players[1].died:
            if False: #CHECK IF ANY BALLS STILL MOVING
                return None
            else:
                # Nothing is moving, someone or both people died, end the game!
                pass #END GAME#####################################
    
    
        # Waiting for someone to take a shot
        if not self.shotTaken:
            # Countdown
            if self.countdown > 0:
                self.countdown -= delta
            else:
                pass # Countdown ran out!
        else:
            pass # Shot was taken
        pass #MAKE THE SHOT HAPPEN############################
        
        
    def resetNextTurn(self):
        self.countdown = 20
        self.shotTaken = False
        
# Not a 'final' class. Function: kind of how the server sets game up for first turn
class FirstTurn(object):
    def __init__(self, playState, screen, buttonManager):
        screenLeft = 0
        screenRight = globals.GAME_WIDTH
        screenCentreX = screenRight/2
        screenBottom = globals.GAME_HEIGHT
        self.screen = screen
        self.buttonManager = buttonManager
        self.playState = playState
    
        # Position players
        #self.playerServer = Crow(screenLeft+64, screenBottom-64, 72, 72)
        #self.playerClient = Crow(screenRight-64-64, screenBottom-64, 72, 72)
        #self.crowGroup = pygame.sprite.Group(self.playerServer, self.playerClient)
        # Group for the divider thing in the middle
        #self.divider = Divider(globals.GAME_WIDTH/2-12, 256, 24, globals.GAME_HEIGHT-256)
        #self.crowBlocker = CrowBlocker(self.divider.x, 0, self.divider.width, globals.GAME_HEIGHT-self.divider.height)
        #self.dividerGroup = pygame.sprite.Group(self.divider, self.crowBlocker)
        
        # Group for the backgrounds and terrain and stuff
        #self.terrainGroup = pygame.sprite.Group()
        # Group for the items
        #self.itemGroup = pygame.sprite.Group()
        # Group for the balls
        self.ball = Ball(5, 500, 0)
        self.ball2 = Ball(680, 300, 0)
        self.ballGroup = pygame.sprite.Group(self.ball, self.ball2)
        
        self.turn = 1 # player 1 is left, player 2 is right
        
        #self.aimArrow = AimArrow(self.playerServer, screen, buttonManager)
        
    def update(self, delta):
        #self.crowGroup.update()
        self.ballGroup.update()
        #if not self.aimArrow.fired:
            #self.aimArrow.update(self.screen, delta)
        #else:
            # Temporary: after a shot a new aimarrow is made
            #self.aimArrow = AimArrow(self.playerServer, self.screen, self.buttonManager)
        self.checkCollisions()
        
        # Remove broken balls
        if self.ball.smash:
            self.ballGroup.remove(self.ball)
        if self.ball2.smash:
            self.ballGroup.remove(self.ball2)
    
    def checkCollisions(self):
        # Make all objects check collisions
        # Crow collision with Divider
        #self.playerServer.dividerCollision(self.divider)
        # Crows collision
        #self.playerServer.crowCollision(self.playerClient)
        # Crow blocker collision
        #self.playerServer.crowBlockerCollision(self.crowBlocker)
        # Ball collision
        #if not self.ball.smash:
        #    self.playerServer.ballCollision(self.ball)
        #    self.playerClient.ballCollision(self.ball)
        #if not self.ball2.smash:
        #    self.playerServer.ballCollision(self.ball2)
        #    self.playerClient.ballCollision(self.ball2)
        # Balls against dividers
        #self.ball.dividerCollision(self.divider)
        #self.ball2.dividerCollision(self.divider)
        # Balls against balls
        #if not self.ball.smash and not self.ball2.smash:
        #    self.ball.ballCollision(self.ball2)
        pass
    
    def draw(self, screen):
        #self.dividerGroup.draw(screen)
        #self.crowGroup.draw(screen)
        #self.ballGroup.draw(screen)
        #if not self.aimArrow.fired:
        #    self.aimArrow.draw(screen)
        
        # Have playState update HUD
        #self.playState.drawHUD(50, 50, 30)
        
    #def keyEvent(self, event):
        #self.aimArrow.keyEvent(event)  
        pass