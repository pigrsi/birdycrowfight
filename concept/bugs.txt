Unfixed bugs:

Mouse clicking to aim/shoot doesn't work sometimes, always works the first time, happens even if keyboard is never used - waiting to implement final aiming system to fix this FIXED: (seems to be fixed when only using one aim arrow)

If you do a tiny jump when standing on the divider, you will fall through it. Not a big deal.

Corner collision doesn't look that nice.

Not really a bug but slamming into a ball against a wall will instantly break it and hurt you. But it could be smooshing a bomb, causing it to explode?

The result of a collision with a ball can push you through the collider.

Not a bug but a design issue, you can't click the knob if it goes off screen

Trying to connect to 127.0.0.0 messes things up